# Changelog for `cursebox`


## Unreleased

### Change "Play from the top" key

It's changed from `G` to `g`, as `G` usually means go to the bottom.

## 1.1.2

### Introduce `random` mode

Makes it easy to load and play random items. For now, only random albums are
supported.

### Change the tag line

That old joke wasn't funny anyway.

## 1.1.1 (2019-01-22)

### Fix problem with empty playlists

There was an error when trying to show an empty playlist. This has now been
fixed, and the general list rendering has been improved to show that the
list is empty, if an empty list is being rendered.

### Introduce the `playlist` argument

Which will make cursebox simply print out the current playlist.

## 1.1 (2018-11-02)

### Introduce `lastfm` mode (beta)

For now, this has just a single feature, but more will be added later on. That
single feature is the possibility to build a playlist based on the loved tracks
from a Last.fm user. The loved tracks list i retrieved, and then each track is
searched for in the LMS library and added to a playlist if found (in random
order). The search is very basic, and relies on the library and Last.fm to
agree on how a track is tagged. E.g. a track that is "featuring ArtistX" may
have this info appended to the artist tag in the libary and the track tag in
Last.fm. In this case, the track will not be found in the library and included
in the loved tracks playlist, even if it's actually in the library.

### Expose `interactive` mode in the usage instructions

When used, a Python REPL is started with an LMS loaded and connected.

This has secretly been there for a while, mostly for debugging purposes. It's
probably still mostly useful for debugging, but there's no reason to hide it.

### Remap search key from `s` to `/`

To comply with the standard keys from Vim.

### Clean up the UI a bit

Streamline how lists are presented. Change how multiple action keys are
presented on the same line. Show action keys in most (if not all?) places. No
more guessing.

### Add underlying support for clearing the current playlist

This is currently not exposed in the UI.

### Fix bug around adding things to the existing playlist

They were loaded instead of added, i.e. the playlist was cleared before adding.

### Improve handling of unknown players

If a Player ID is specified when starting Cursebox, but the specified player is
unavailable (disconnected), show a warning message along with the player
selection menu, instead of crashing hard.

## 1.0.17 (2018-07-24)

### Improve time formatting in playlist

If one or more tracks are 10+ minutes long, make sure the time of all tracks
is formatted with 2 digits for the minutes, to make sure the listing is
beautifully lined up.

### Improve track progress highlight

Instead of using underline as the indicator, invert the colours in
the progression.

### Improve the splash screen

The logo is now animated as the last part of the splash screen, after
connection to LMS has been established.


## 1.0.16 (2018-07-17)

### Toggle play/pause from the playlist screen

Via the Space bar.

## Allow removing tracks from the playlist screen

It's now possible to delete the highlighted track on the playlist screen by
pressnig the `D` key.

## Allow rearranging tracks on the playlist.

Highligted track can now be moved down/up by pressing the `J`/`K` keys.

## Navigate New Music, Favorites and Player Selection with `j`/`k`

This used to be (`Shift` +) `Tab`, but we want to use `j`/`k` navigation
where possible.


## 1.0.15 (2018-04-19)

### Fix a bug in the improved data handling of 1.0.14

Forgot to parse value as `float` instead of `int`.


## 1.0.14 (2018-04-19)

### Reduce splash screen time

Instead of forcing the splash screen to be shown for at least a second, this is
now at least half a second. Nobody wants their tools to be intentionally slow.
This will drastically reduce the relative time needed to perform small tasks
(e.g. pause/play) while ensuring that the splash screen is still recognised by
the user.

### Improve handling of Now Playing data.

Don't assume LMS will always return values for the various data elements for
Now Playing data. If the player doesn't have anything on the playlist, no
values are returned. This made Cursebox crash on empty playlists.


## 1.0.13 (2018-03-13)

### Improve the Start screen

 * When the playlist has been played to the end, show the "Play from the top"
   item before the regular "Play" item (instead of after), as that is a more
   likely preferred action than playing the last track again.
 * Show time and duration of currently playing track. Still a bit sluggish, but
   way better than nothing.

### Improve the Current Playlist screen

 * Available commands are now shown. No more guessing.
 * The keys for highlight navigation has been changed from `Tab`/`Shift+Tab` to
   `j`/`k`, to standardise navigation in non text input screens.
 * The key for playing the highlighted track has been changed from `Enter` to
   `p`, to standardise keys across screens.
 * Show duration of tracks as part of the playlist.


## 1.0.12 (2018-02-27)

### Fix player selection

When changing player to control, the last of the players returned from LMS was
not added to the list of players.

### Improve the Start screen

Until now, the start screen did not change (i.e. refresh the data shown) until
you returned to it from another screen. This has now been changed to refresh
every second.

### Add play progress to Now Playing

On the start screen, the Now Playing info is now partially underlined,
according to the play progress of the current track.


## 1.0.11 (2018-02-26)

### Fix wrong information in usage information (and README)

This said that version checks are done to `gitlab.com`, but this was changed to
`pypi.python.org` in `1.0.10`.

### Add `-v`/`--version` flag

This will print the current version of Cursebox.

## 1.0.10 (2018-02-26)

### Improve navigation

When navigating down the Contributor > Album > Track hierarchy from Search,
allow the user to go back to the previous level, instead of just being sent
back to the Home screen.

The same goes for the Album Screen accessed from the New Music listing. This
now takes the user back to the New Music listing (remembering the highlight
position) instead of taking the user back to Home.

### Improve online version check

Instead of getting version information from the code repository at GitLab,
which may not report the correct version, get the information from PyPI, which
is the official source of installation and should report the actually
available version.


## 1.0.9 (2018-02-15)

### Add (optional) online version check

In order to help users keep their version of Cursebox up to date, a new option
(`-V`, `--check_version`) is introduced, that -- if enabled (disabled by
default) -- will make Cursebox check online whether a newer version
is available.


## 1.0.8 (2018-02-15)

### Improve the playlist action screen

When an item (contributor, album, track) has been selected to either add to
current playlist or play immediately, details about the item is now shown as
part of this screen.


## Before this

Only our forefathers knew what happened before this.
